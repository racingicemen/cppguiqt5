# CPPGUIQT5

This repository contains the source from C++ GUI Programming with Qt4, 2nd Edition, 
by Blanchetter and Summerfeld, ported to Qt5 (5.11 at this time).

Most of the Qt5 C++ books seem to focus on the QML aspects of Qt5, which, if 
I understand correctly, is the big change from Qt 4.3 to Qt5. The C++ parts should 
be almost the same, and this reposistory explores this notion and records any 
changes required to go from Qt 4.3 to Qt 5.

The Layout of the repository is chapter by chapter, and within each chapter folder, 
we have a sub-folder for each listing/example.
