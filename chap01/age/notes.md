## Qt4 to Qt5 change
1. Had to add ```QT += widgets``` to age.pro

## Git related changes
1. Set TARGET=age.exe so that .gitignore can be used to exclude binaries
