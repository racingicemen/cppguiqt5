## Qt4 to Qt5 change
1. Had to add ```QT += widgets``` to quit.pro

## Git related changes
1. Set TARGET=quit.exe so that .gitignore can be used to exclude binaries
